package se331.lab.rest.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;

@Controller
public class StudentController {
    List<Student> students;
    public StudentController() {
        this.students = new ArrayList<>();
        this.students.add(Student.builder()
                .id(Long.valueOf(1))
                .studentId("SE-001")
                .name("Chamnol")
                .surname("Yin")
                .gpa(3.59)
                .image("http://34.218.246.148:8190/images/hazard.jpg")
                .penAmount(0)
                .description("Best man!!!!")
                .build());
    }

    @GetMapping("/students")
    public ResponseEntity getAllStudent(){
        return ResponseEntity.ok(students);
    }

    @GetMapping("/students/{id}")
    public ResponseEntity getStudentById(@PathVariable("id")Long id){
        return ResponseEntity.ok(students.get(Math.toIntExact(id-1)));
    }

    @PostMapping("/students")
    public ResponseEntity saveStudent(@RequestBody Student student){
        student.setId((long) this.students.size()+1);
        this.students.add(student);
        return ResponseEntity.ok(student);
    };
}
